package com.ondevio.auth.core.services;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import com.ondevio.auth.core.userdetails.CustomUserDetails;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
public class AuthenticationPrincipalDefaultService implements AuthenticationPrincipalService {

	@Override
	public UserDetails getUser(Authentication auth) {
		if (auth == null) {
			return null;
		}
		return (UserDetails)auth.getPrincipal();
	}

	@Override
	public String getUserExternalIdAsString(Authentication auth) {
		UserDetails user = this.getUser(auth);
		if (user == null) {
			return null;
		}
		String id = null;
		try {
			CustomUserDetails custom = (CustomUserDetails) user;
			id = custom.getExternalId();
		} catch (ClassCastException e) {
			log.error("No se ha podido obtener el externalId porque el usuario autenticado no es del tipo CustomUserDetails", e);
		}
		return id;
	}

	@Override
	public Long getUserExternalIdAsLong(Authentication auth) {
		String externalIdAsString = this.getUserExternalIdAsString(auth);
		if (externalIdAsString == null) {
			return null;
		}
		Long id = null;
		try {
			id = Long.parseLong(externalIdAsString);	
		} catch (NumberFormatException e) {
			log.error("No se pudo convertir a Long el externalId: " + externalIdAsString);
		}
		return id;
	}

}
