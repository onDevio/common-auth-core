package com.ondevio.auth.core.services;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Esta interfaz suele tener 2 implementaciones:
 * 1. Una implementacion "real" que obtiene el User a partir del Authentication
 * 2. Una implementacion "dummy" que devuelve un usuario fijo aunque el Authentication sea null
 * 
 * La implementación "Dummy" es útil para trabajar sin activar Spring Security, con el profile "nosec".
 * Cuando "nosec" está activo, el parámetro Authentication llega a null a los @Contoller.
 * Es ese caso el "Dummy" debe ignorar el auth nulo y devolver un UserDetails fijo.
 * 
 * @author bigpapa
 *
 */
public interface AuthenticationPrincipalService {

	UserDetails getUser(Authentication auth);
	
	String getUserExternalIdAsString(Authentication auth);
	
	Long getUserExternalIdAsLong(Authentication auth);

}
