package com.ondevio.auth.core.userdetails;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@SuppressWarnings("serial")
@Setter
@Getter
public class CustomUserDetails extends org.springframework.security.core.userdetails.User {

	private final String externalId;
	
	public CustomUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, String externalId) {
		super(username, password == null ? "": password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.externalId = externalId;
		if (password == null) {
			this.eraseCredentials();
		}
	}
	
	@JsonIgnore
	public Long getExternalIdAsLong() {
		if (this.externalId == null) {
			return null;
		}
		
		Long id = null ;
		try {
			id = Long.parseLong(this.externalId);
		} catch (java.lang.NumberFormatException e) {
			log.debug("Error al convertir a Long el externalId: " + this.externalId);
		}
		return id;
	}
	
}