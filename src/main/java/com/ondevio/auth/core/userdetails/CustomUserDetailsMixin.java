package com.ondevio.auth.core.userdetails;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomUserDetailsMixin {
	@JsonCreator
	public CustomUserDetailsMixin(
			@JsonProperty("username") String username, 
			@JsonProperty("password") String password, 
			@JsonProperty("enabled") boolean enabled, 
			@JsonProperty("accountNonExpired") boolean accountNonExpired,
			@JsonProperty("credentialsNonExpired") boolean credentialsNonExpired, 
			@JsonProperty("accountNonLocked") boolean accountNonLocked,
			@JsonProperty("authorities") Collection<? extends GrantedAuthority> authorities, 
			@JsonProperty("externalId") String externalId) {
		System.out.println("Wont be called");
	}
}